<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codm1 extends Model
{
    protected $table = 'codm1s';
    protected $fillable = ['team_1','team_2'];
}
