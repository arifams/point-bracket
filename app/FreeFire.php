<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeFire extends Model
{
    protected $table = 'freefires';
    protected $fillable = ['team','point'];
}
