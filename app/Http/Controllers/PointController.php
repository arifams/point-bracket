<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Point;
 
class PointController extends Controller
{
 
    public function index()
    {
    	$point = Point::all();
    	return view('point', ['point' => $point]);
    }

    public function tambah()
    {
    	return view('tambah');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama_tim' => 'required',
    		'point' => 'required'
    	]);
 
        Pegawai::create([
    		'nama_tim' => $request->nama_tim,
    		'point' => $request->point
    	]);
 
    	return redirect('/point');
    }
}