<?php

namespace App\Http\Controllers;

use App\Common;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('admin.common.index');
    }

    public function create(){
        return view('admin.common.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{

            $common = new Common();

            $common->text = $request->input('text');

            $common->save();
            DB::commit();
            return view('admin.common.create')->with('common',$common)->withInfo('Live text berhasil ditambahkan');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.common.create')->with('common',$common)->withInfo('Live text gagal ditambahkan');
        }
    }

    public function edit($id){
        $common = Common::find($id);
        // dd($product);
        return view('admin.common.edit', compact('common'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'text' => 'required',
            ]);
            $common = Common::find($id);
            Common::where('id', $request->id)->update([
            'text' => $request->text,

            ]);

            $common->save();
            DB::commit();
            return view('admin.common.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $common = Common::where('id', $id)->first();

        $common->delete();
        return redirect()->back();
    }

}
