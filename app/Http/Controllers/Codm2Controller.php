<?php

namespace App\Http\Controllers;

use App\Codm2;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm2Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm2.index');
    }

    public function show()
    {
        return view('admin.codm.codm2.index');
    }

    public function create(){
        return view('admin.codm.codm2.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            // $this->validate($request, [
            //     'team_1' => 'required',
            //     'team_2' => 'required',
            // ]);
            $codm2 = new Codm2();

            $codm2->team_1 = $request->input('team_1');
            $codm2->team_2 = $request->input('team_2');

            $codm2->save();
            DB::commit();
            return view('admin.codm.codm2.create')->with('codm2',$codm2)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm2.create')->with('codm2',$codm2)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm2 = Codm2::find($id);
        // dd($product);
        return view('admin.codm.codm2.edit', compact('codm2'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm2 = Codm2::find($id);
            Codm2::where('id', $request->id)->update([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,

            ]);

            $codm2->save();
            DB::commit();
            return view('admin.codm.codm2.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm2 = Codm2::where('id', $id)->first();

        $codm2->delete();
        return redirect()->back();
    }

}
