<?php

namespace App\Http\Controllers;

use App\Codm6;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm6Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm6.index');
    }

    public function show()
    {
        return view('admin.codm.codm6.index');
    }

    public function create(){
        return view('admin.codm.codm6.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            // $this->validate($request, [
            //     'team_1' => 'required',
            //     'team_2' => 'required',
            // ]);
            $codm6 = new Codm6();

            $codm6->team_1 = $request->input('team_1');

            $codm6->save();
            DB::commit();
            return view('admin.codm.codm6.index')->with('codm6',$codm6)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm6.create')->with('codm6',$codm6)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm6 = Codm6::find($id);
        // dd($product);
        return view('admin.codm.codm6.edit', compact('codm6'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
            ]);
            $codm6 = Codm6::find($id);
            Codm6::where('id', $request->id)->update([
            'team_1' => $request->team_1,

            ]);

            $codm6->save();
            DB::commit();
            return view('admin.codm.codm6.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm6 = Codm6::where('id', $id)->first();

        $codm6->delete();
        return redirect()->back();
    }

}
