<?php

namespace App\Http\Controllers;

use App\Codm3;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm3Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm3.index');
    }

    public function show()
    {
        return view('admin.codm.codm3.index');
    }

    public function create(){
        return view('admin.codm.codm3.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            // $this->validate($request, [
            //     'team_1' => 'required',
            //     'team_2' => 'required',
            // ]);
            $codm3 = new Codm3();

            $codm3->team_1 = $request->input('team_1');
            $codm3->team_2 = $request->input('team_2');

            $codm3->save();
            DB::commit();
            return view('admin.codm.codm3.create')->with('codm3',$codm3)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm3.create')->with('codm3',$codm3)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm3 = Codm3::find($id);
        // dd($product);
        return view('admin.codm.codm3.edit', compact('codm3'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm3 = Codm3::find($id);
            Codm3::where('id', $request->id)->update([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,

            ]);

            $codm3->save();
            DB::commit();
            return view('admin.codm.codm2.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm3 = Codm3::where('id', $id)->first();

        $codm3->delete();
        return redirect()->back();
    }

}
