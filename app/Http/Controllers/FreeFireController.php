<?php

namespace App\Http\Controllers;

use App\FreeFire;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FreeFireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('freefire.index');
    }

    public function show()
    {
        // dd($id);
        // $freefire = FreeFire::find($id);
        return view('admin.freefire.index');
    }

    public function create(){
        return view('admin.freefire.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team' => 'required',
                'point' => 'required',
            ]);
            $freefire = new FreeFire();

            $freefire->team = $request->input('team');
            $freefire->point = $request->input('point');

            $freefire->save();
            DB::commit();
            return view('admin.freefire.create')->with('freefire',$freefire)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.freefire.create')->with('freefire',$freefire)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $freefire = FreeFire::find($id);
        // dd($product);
        return view('admin.freefire.edit', compact('freefire'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $freefire = FreeFire::find($id);
            FreeFire::where('id', $request->id)->update([
            'team' => $request->team,
            'point' => $request->point,

            ]);

            $freefire->save();
            DB::commit();
            return view('admin.freefire.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $freefire = FreeFire::where('id', $id)->first();

        $freefire->delete();
        return redirect()->back();
    }

}
