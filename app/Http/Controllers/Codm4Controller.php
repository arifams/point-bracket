<?php

namespace App\Http\Controllers;

use App\Codm4;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm4Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm4.index');
    }

    public function show()
    {
        return view('admin.codm.codm4.index');
    }

    public function create(){
        return view('admin.codm.codm4.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            // $this->validate($request, [
            //     'team_1' => 'required',
            //     'team_2' => 'required',
            // ]);
            $codm4 = new Codm4();

            $codm4->team_1 = $request->input('team_1');
            $codm4->team_2 = $request->input('team_2');

            $codm4->save();
            DB::commit();
            return view('admin.codm.codm4.create')->with('codm4',$codm4)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm4.create')->with('codm4',$codm4)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm4 = Codm4::find($id);
        // dd($product);
        return view('admin.codm.codm4.edit', compact('codm4'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm4 = Codm4::find($id);
            Codm4::where('id', $request->id)->update([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,

            ]);

            $codm4->save();
            DB::commit();
            return view('admin.codm.codm4.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm4 = Codm4::where('id', $id)->first();

        $codm4->delete();
        return redirect()->back();
    }

}
