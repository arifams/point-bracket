<?php

namespace App\Http\Controllers;

use App\Codm1;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm1Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm1.index');
    }

    public function show()
    {
        return view('admin.codm.codm1.index');
    }

    public function create(){
        return view('admin.codm.codm1.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm1 = new Codm1();

            $codm1->team_1 = $request->input('team_1');
            $codm1->team_2 = $request->input('team_2');

            $codm1->save();
            DB::commit();
            return view('admin.codm.codm1.create')->with('codm1',$codm1)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm1.create')->with('codm1',$codm1)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm1 = Codm1::find($id);
        // dd($product);
        return view('admin.codm.codm1.edit', compact('codm1'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm1 = Codm1::find($id);
            Codm1::where('id', $request->id)->update([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,

            ]);

            $codm1->save();
            DB::commit();
            return view('admin.codm.codm1.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm1 = Codm1::where('id', $id)->first();

        $codm1->delete();
        return redirect()->back();
    }

}
