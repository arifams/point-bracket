<?php

namespace App\Http\Controllers;

use App\Codm5;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Codm5Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('codm5.index');
    }

    public function show()
    {
        return view('admin.codm.codm5.index');
    }

    public function create(){
        return view('admin.codm.codm5.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            // $this->validate($request, [
            //     'team_1' => 'required',
            //     'team_2' => 'required',
            // ]);
            $codm5 = new Codm5();

            $codm5->team_1 = $request->input('team_1');
            $codm5->team_2 = $request->input('team_2');

            $codm5->save();
            DB::commit();
            return view('admin.codm.codm5.create')->with('codm5',$codm5)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.codm.codm5.create')->with('codm5',$codm5)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $codm5 = Codm5::find($id);
        // dd($product);
        return view('admin.codm.codm5.edit', compact('codm5'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team_1' => 'required',
                'team_2' => 'required',
            ]);
            $codm5 = Codm5::find($id);
            Codm5::where('id', $request->id)->update([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,

            ]);

            $codm5->save();
            DB::commit();
            return view('admin.codm.codm5.index');
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $codm5 = Codm5::where('id', $id)->first();

        $codm5->delete();
        return redirect()->back();
    }

}
