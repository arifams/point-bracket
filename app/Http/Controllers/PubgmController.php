<?php

namespace App\Http\Controllers;

use App\Pubgm;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PubgmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pubgm.index');
    }

    public function show()
    {
        // dd($id);
        // $pubgm = pubgm::find($id);
        return view('admin.pubgm.index');
    }

    public function create(){
        return view('admin.pubgm.create');
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'team' => 'required',
                'point' => 'required',
            ]);
            $pubgm = new Pubgm();

            $pubgm->team = $request->input('team');
            $pubgm->point = $request->input('point');

            $pubgm->save();
            DB::commit();
            return view('admin.pubgm.create')->with('pubgm',$pubgm)->withInfo('Team berhasil ditambah');
        }catch(Exception $e){
            DB::rollback();
            return view('admin.pubgm.create')->with('pubgm',$pubgm)->withInfo('Team gagal ditambah');
        }
    }

    public function edit($id){
        $pubgm = Pubgm::find($id);
        // dd($product);
        return view('admin.pubgm.edit', compact('pubgm'));
    }

    public function update(Request $request,$id){
        // dd($request);
        DB::beginTransaction();
        try{
            $pubgm = Pubgm::find($id);
            Pubgm::where('id', $request->id)->update([
            'team' => $request->team,
            'point' => $request->point,

            ]);

            $pubgm->save();
            DB::commit();
            return back()->withInput();
            }catch(Exception $e){
            DB::rollback();
            return back()->withInput();
            }
        }

    public function delete($id){
        $pubgm = Pubgm::where('id', $id)->first();

        $pubgm->delete();
        return redirect()->back();
    }

    public function tambah(Request $request){
        // $pubgm = Pubgm::find($id);
        // dd($product);
        // DB::table('pubgm')->where('team',$team->team)-update([
        //     'point' => $team->team
        // ]);
        // dd($request);

        $posisi = [1,2,3,4,5,6,7,8,9,10];
        return view('admin.pubgm.tambah',compact('posisi'));
    }

}
