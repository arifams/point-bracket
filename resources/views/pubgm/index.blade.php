<?php
use App\Pubgm;
$pubgm = Pubgm::orderBy('point', 'desc')->paginate(50);
$no = 1;
?>
@extends('layouts.master')
@section('style')

@endsection
@include('layouts.navbar')
<br><br><br><br><br><br>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                </div>

                <div class="container">
                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Team</th>
                            <th scope="col">Point</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach($pubgm as $pubgm)
                            <th scope="row">{{$no++}}</th>
                            <td>{{$pubgm->team}}</td>
                            <td>{{$pubgm->point}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
