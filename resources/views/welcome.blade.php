<!DOCTYPE html>
<?php
// use App\Common;
// $common = Common::first();
use App\Common;
$commons = Common::orderBy('id', 'desc')->get();
?>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Portal Gaming Indonesia Turnamen</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                /* background-color: #fff; */
                /* color: #636b6f; */
                /* font-family: 'Nunito', sans-serif; */
                /* font-weight: 200; */
                height: 100vh;
                margin: 0;
            }

            .topnav {
                overflow: hidden;
                background-color: #333;
                position: sticky;
                top: 0;
                width: 100%;
            }

            .topnav a{
                float: left;
                display: block;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }

            .topnav a:hover {
                background-color: #ddd;
                color: black;
            }

            .topnav a,active {
                background-color: #333;
                color: white;
            }

            .box{
                width: 100%;
                text-align:center;
                }

            .runningtextbox {
            position: fixed;
            background: #546eff;
            bottom: 0;
            width: 100%;
            padding: 10px;
            margin: 0px;
            }

            .topnav b{
                float: right;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }

            .topnav b:hover {
                background-color: #ddd;
                color: black;
            }

            .topnav b,active {
                background-color: #333;
                color: white;
            }

            .main{
                padding: 16px;
                margin-top: 30px;
                height: 1500px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .container {
                position: relative;
                text-align: center;
                /* color: white; */
            }

            .centered {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }

            .content {
                text-align: center;
                padding-top: 16px;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }




        .card{
            margin-bottom:20px;
        }

        .card-title{
            margin-top:10px;
            font-weight: ;
        }

        .myjumbo{
            background-image: url("/images/juggernaut-dota-2-8d.jpg");
            background-size: cover;
        }
        </style>
    </head>
    <body>
    @include('layouts.navbar')
    <br><br><br>
    <div class="jumbotron myjumbo">
        <div class="container">
          <h1 style="color:white">PGID Tournament</h1>
          {{-- <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first projects on the web.</p> --}}
        </div>
      </div>

    <div class="container">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/live_stream?channel=ImsvVIN83lQXMCljNYytOA" frameborder="0" allowfullscreen></iframe>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="card-block">
                        <img src="{{ asset('uploads/cover/ml.jpg') }}" alt="" class="img-fluid">
                        <div class="card-title">
                            <h4>Mobile Legends Tournament Ngabuburit Online</h4>
                        </div>
                        <div class="card-text">Mobile Legends</div>
                        <div class="card-text">Participant</div>
                        <div class="card-text">1-3 Mei 2020</div>
                        <a style="margin-top:10px" href="{{route('guest.codm.index')}}" class="btn btn-dark btn-block">
                            Lihat
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="card-block">
                        <img src="{{ asset('uploads/cover/pubg.jpg') }}" alt="" class="img-fluid">
                        <div class="card-title">
                            <h4>PUBG Mobile Tournament Ngabuburit Online</h4>
                        </div>
                        <div class="card-text">PUBG Mobile</div>
                        <div class="card-text">Participant</div>
                        <div class="card-text">8-10 Mei 2020</div>
                        <a style="margin-top:10px" href="{{route('guest.pubgm.index')}}" class="btn btn-dark btn-block">
                            Lihat
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        {{-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

        <div class="container-fluid">
            <div class="content">
                <div class="title m-b-md">
                    Portal Gaming Indonesia Tournament
                </div>

                <div class="links">
                    <a href={{route('guest.pubgm.index')}}>PUBG Mobile</a>
                    <a href={{route('guest.codm.index')}}>Mobile Legends</a>
                </div>
            </div>
        </div>
    </div> --}}
    <br><br><br><br><br><br>
    
    <div class="runningtextbox"><marquee direction="left" scrollamount="5" behavior="scroll" style="width: 100%; height: 15px; color: #ffffff; font-size: 13px; background-color: #546eff;">
    @foreach($commons as $common)
    <p>{{$common->text}} </p>
    @endforeach
    </marquee></div>


    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
