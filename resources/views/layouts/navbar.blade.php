<style>
    .inverted{
        filter: invert(100%);
    }
</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">
            <img class="inverted img-responsive" src="{{ asset('/images/portalgaming-logo-header.png') }}" alt="">
          </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="https://www.portalgaming.id/">PGID
                  <span class="sr-only">(current)</span>
                </a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="{{url('/')}}">All Tournament</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.portalgaming.id/category/news/">News</a>
          </li>
        </ul>
      </div>
    </div>
</nav>
