<?php
use App\FreeFire;
$freefires = FreeFire::orderBy('point', 'desc')->paginate(50);
$no = 1;
?>
@extends('layouts.master')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                </div>

                <div class="container">
                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Team</th>
                            <th scope="col">Point</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach($freefires as $freefire)
                            <th scope="row">{{$no++}}</th>
                            <td>{{$freefire->team}}</td>
                            <td>{{$freefire->point}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
