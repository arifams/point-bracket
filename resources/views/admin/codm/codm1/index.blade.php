<?php
use App\Codm1;
$codm1s = Codm1::orderBy('id', 'desc')->get();
$no = 1;
?>
@extends('admin.layouts.master')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mobile Legends Area Round 1
                <a href="{{route('admin.codm1.create')}}" class="btn btn-primary btn-sm">
                    Tambah Team
                </a>
                </div>
                <div class="card card-plain table-plain-bg">
                    <div class="card-header ">
                        <h4 class="card-title">Data Team Mobile Legend Round 1</h4>
                    </div>
                    <div class="card-body table-full-width table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Team 1</th>
                                    <th scope="col">Team 2</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($codm1s as $codm1)
                                    <tr>
                                        <th scope="row">{{$no++}}</th>
                                        <td>{{$codm1->team_1}}</td>
                                        <td>{{$codm1->team_2}}</td>
                                        <td>
                                            <td>
                                                <a href="{{route('admin.codm1.show')}}/{{$codm1->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                            </td>
                                            <td>
                                                <form action="{{route('admin.codm1.show')}}/{{ $codm1->id }}" method="post">
                                                    {{ method_field('DELETE')}}
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin akan menghapus team ini ?')">Delete</button>
                                                </form>
                                            </td>
                                        </td>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
