@extends('admin.layouts.master')

@section('content')

<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.codm6.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Champion</label>
                    <input type="text" id="team_1" name="team_1" value="{{ old('team_1') }}" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
