@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.codm6.update',$codm6->id) }}" method="POST" enctype="multipart/form-data">
            @method('patch')
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Nama Team</label>
                    <input type="text" id="team" name="team_1" value="{{$codm6->team_1}}" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
