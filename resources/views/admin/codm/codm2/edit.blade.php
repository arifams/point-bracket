@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.codm2.update',$codm2->id) }}" method="POST" enctype="multipart/form-data">
            @method('patch')
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Team 1</label>
                    <input type="text" id="team_1" name="team_1" value="{{$codm2->team_1}}" class="form-control">
                </div>
                <div class="form-group">
                    <label class="bmd-label-floating">Team 2</label>
                    <input type="text" id="team_2" name="team_2" value="{{$codm2->team_2}}" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
