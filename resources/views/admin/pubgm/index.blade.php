<?php
use App\Pubgm;
$pubgms = Pubgm::orderBy('point', 'desc')->paginate(50);
$no = 1;
?>
@extends('admin.layouts.master')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">PUBG Mobile Area
                <a href="{{route('admin.pubgm.create')}}" class="btn btn-primary btn-sm">
                    Tambah Team
                </a>
                <a href="{{route('admin.pubgm.tambah')}}" class="btn btn-primary btn-sm">
                    Tambah Point
                </a>
                </div>
                <div class="card card-plain table-plain-bg">
                    <div class="card-header ">
                        <h4 class="card-title">Data Team PUBG Mobile</h4>
                        {{-- <p class="card-category">Here is a subtitle for this table</p> --}}
                    </div>
                    <div class="card-body table-full-width table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <th scope="col">No</th>
                                <th scope="col">Nama Team</th>
                                <th scope="col">Point</th>
                                <th scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach($pubgms as $pubgm)
                                    <tr>
                                        <th scope="row">{{$no++}}</th>
                                        <td>{{$pubgm->team}}</td>
                                        <td>{{$pubgm->point}}</td>
                                        <td>
                                            <td>
                                                <a href="{{route('admin.pubgm.show')}}/{{$pubgm->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                            </td>
                                            <td>
                                                <form action="{{route('admin.pubgm.show')}}/{{ $pubgm->id }}" method="post">
                                                    {{ method_field('DELETE')}}
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin akan menghapus baris ini ?')">Delete</button>
                                                </form>
                                            </td>
                                        </td>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
