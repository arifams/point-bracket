<?php
use App\Pubgm;
$pubgms = Pubgm::orderBy('point', 'desc')->paginate(50);
$no = 1;
?>
@extends('admin.layouts.master')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <form action="{{ route('admin.pubgm.store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-header">Atur Posisi</div>
                <div class="container">
                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                        <!-- <th scope="col">No</th> -->
                            <th scope="col">Posisi</th>
                            <th scope="col">Team</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- diloop disini --}}
                        <tr>
                        @foreach($posisi as $posisi)
                            <!-- <td scope="row">{{$no++}}</td> -->
                            <td>Posisi Ke {{$posisi}}</td>
                            <td>
                                <select id="team">
                                    @foreach($pubgms as $pubgm)
                                    <option value="{{$pubgm->team}}">{{$pubgm->team}}</option>
                                    @endforeach
                                </select>
                            </td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
