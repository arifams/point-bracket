@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.pubgm.update',$pubgm->id) }}" method="POST" enctype="multipart/form-data">
            @method('patch')
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Nama Team</label>
                    <input type="text" id="team" name="team" value="{{$pubgm->team}}" class="form-control">
                </div>
                <div class="form-group">
                    <label class="bmd-label-floating">Point</label>
                    <input type="text" id="point" name="point" value="{{$pubgm->point}}" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
