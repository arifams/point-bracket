@extends('admin.layouts.master')
@section('style')
<style>
    .textarea {
        width: 300px;
        height: 150px;
    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.common.update',$common->id) }}" method="POST" enctype="multipart/form-data">
            @method('patch')
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Text</label>
                    <textarea class="form-control textarea" id="text" name="text" value="{{$common->text}}"id="exampleFormControlTextarea1" rows="3">{{$common->text}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
