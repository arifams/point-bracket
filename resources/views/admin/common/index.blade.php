<?php
use App\Common;
$commons = Common::orderBy('id', 'desc')->get();
$no = 1;
?>
@extends('admin.layouts.master')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <a href="{{route('admin.common.create')}}" class="btn btn-primary btn-sm">
                    Add Live Text
                </a>
                </div>
                <br>
                <div class="container">
                    <div class="card card-plain table-plain-bg">
                        <div class="card-header ">
                            <h4 class="card-title">Live Text Area</h4>
                            <p class="card-category">Here is a text for marquee</p>
                        </div>
                        <div class="card-body table-full-width table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th scope="col">No</th>
                                    <th scope="col">Live Text</th>
                                </thead>
                                <tbody>
                                    @foreach($commons as $common)
                                        <tr>
                                            <th scope="row">{{$no++}}</th>
                                            <td><marquee>{{$common->text}}</marquee></td>
                                            <td>
                                                <td>
                                                    <a href="{{route('admin.common.show')}}/{{$common->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{route('admin.common.show')}}/{{ $common->id }}" method="post">
                                                        {{ method_field('DELETE')}}
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin akan menghapus live text ini ?')">Delete</button>
                                                    </form>
                                                </td>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
