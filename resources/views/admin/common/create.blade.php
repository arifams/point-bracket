<?php
use App\Common;
$common = Common::first();
?>
@extends('admin.layouts.master')
@section('style')
<style>
    .textarea {
        width: 300px;
        height: 150px;
    }
</style>
@endsection
@section('content')

<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.common.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Live Text</label>
                    <textarea class="form-control textarea" id="text" name="text" value="{{ old('text') }}"id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
