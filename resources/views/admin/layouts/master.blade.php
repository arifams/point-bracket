 <!DOCTYPE html>

 <html lang="en">

 <head>
     <meta charset="utf-8" />
     {{-- <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"> --}}
     <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <title>PGID admin site</title>
     <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
     <!--     Fonts and icons     -->
     {{-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" /> --}}
     <!-- CSS Files -->
     <link href="{{asset('../assets/css/bootstrap.min.css')}}" rel="stylesheet" />
     <link href="{{asset('../assets/css/light-bootstrap-dashboard.css?v=2.0.0 ')}}" rel="stylesheet" />

 </head>
    @yield('style')
 <body>
     <div class="wrapper">
         <div class="sidebar" data-color="black">
             @include('admin.layouts.sidebar')
         </div>
         <div class="main-panel">
             @include('admin.layouts.header')
                <div class="content">
                    @yield('content')
                </div>
             <footer class="footer">
                 @include('admin.layouts.footer')
             </footer>
         </div>
     </div>
 </body>
 <!--   Core JS Files   -->
 <script src="{{asset('../assets/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
 <script src="{{asset('../assets/js/core/popper.min.js')}}" type="text/javascript"></script>
 <script src="{{asset('../assets/js/core/bootstrap.min.js')}}" type="text/javascript"></script>

 <script type="text/javascript">
 </script>

 </html>
