
    <div class="sidebar-wrapper" data-color="azure">
        <div class="logo">
            <a class="simple-text">
                PGID
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('admin.common.show') }}">
                    <i class="nc-icon nc-notification-70"></i>
                    <p>Live Text</p>
                </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('admin.pubgm.show') }}">
                    <i class="nc-icon nc-controller-modern"></i>
                    <p>PUBG Mobile</p>
                </a>
            </li>
            <li class="nav-item active dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="nc-icon nc-controller-modern"></i>
                    <p>Mobile Legends</p> <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('admin.codm1.show') }}">
                        ML Round 1
                    </a>
                    <a class="dropdown-item" href="{{ route('admin.codm2.show') }}">
                        ML Round 2
                    </a>
                    <a class="dropdown-item" href="{{ route('admin.codm3.show') }}">
                        ML Quarter Final
                    </a>
                    <a class="dropdown-item" href="{{ route('admin.codm4.show') }}">
                        ML Semi Final
                    </a>
                    <a class="dropdown-item" href="{{ route('admin.codm5.show') }}">
                        ML Final
                    </a>
                    <a class="dropdown-item" href="{{ route('admin.codm6.show') }}">
                        ML Champion
                    </a>

                    {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form> --}}
                </div>
            </li>

        </ul>
    </div>

