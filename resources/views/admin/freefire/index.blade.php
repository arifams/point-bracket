<?php
use App\FreeFire;
$freefires = FreeFire::orderBy('point', 'desc')->paginate(50);
$no = 1;
?>
@extends('admin.layouts.app')
@section('style')
<!-- css bisa disini -->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Free Fire Area
                <a href="{{route('admin.freefire.create')}}" class="btn btn-success">
                    Tambah Team
                </a>
                </div>

                <div class="container">
                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Team</th>
                            <th scope="col">Point</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- diloop disini --}}
                        <tr>
                        @foreach($freefires as $freefire)
                            <th scope="row">{{$no++}}</th>
                            <td>{{$freefire->team}}</td>
                            <td>{{$freefire->point}}</td>
                            <td>
                                <td>
                                    <a href="{{route('admin.freefire.show')}}/{{$freefire->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.freefire.show')}}/{{ $freefire->id }}" method="post">
                                        {{ method_field('DELETE')}}
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin akan menghapus baris ini ?')">Delete</button>
                                    </form>
                                </td>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
