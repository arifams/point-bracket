@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="card-body">
        <form action="{{ route('admin.freefire.update',$freefire->id) }}" method="POST" enctype="multipart/form-data">
            @method('patch')
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Nama Team</label>
                    <input type="text" id="team" name="team" value="{{$freefire->team}}}" class="form-control">
                </div>
                <div class="form-group">
                    <label class="bmd-label-floating">Point</label>
                    <input type="text" id="point" name="point" value="{{$freefire->point}}" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
@endsection
