<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Tournament</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Sistem Point Tournament
                </div>
                <div class="card-body">
                    <a href="/point/tambah" class="btn btn-primary">Input Tim Baru</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Nama Tim</th>
                                <th>Point</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($point as $p)
                            <tr>
                                <td>{{ $p->nama_tim }}</td>
                                <td>{{ $p->point }}</td>
                                <td>
                                    <a href="/point/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                                    <a href="/point/hapus/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>