<?php
use App\Codm1;
use App\Codm2;
use App\Codm3;
use App\Codm4;
use App\Codm5;
use App\Codm6;
use App\Common;
$commons = Common::orderBy('id', 'desc')->get();
$codm1s = Codm1::orderBy('id', 'desc')->get();
$codm2s = Codm2::orderBy('id', 'desc')->get();
$codm3s = Codm3::orderBy('id', 'desc')->get();
$codm4s = Codm4::orderBy('id', 'desc')->get();
$codm5s = Codm5::orderBy('id', 'desc')->get();
$codm6s = Codm6::orderBy('id', 'desc')->get();
$no = 1;
?>
@extends('layouts.ml')

@section('style')
<head>
<style>
/* @import 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700'; */
   /*
 *  Flex Layout Specifics
*/
    main{
    margin-top:40px;
    display:flex;
    flex-direction:row;
    }
    .round{
    display:flex;
    flex-direction:column;
    justify-content:center;
    width:200px;
    list-style:none;
    padding:0;
    background-color: rgba(0, 0, 0, 0.80);
    }
  .round .spacer{ flex-grow:1; }
  .round .spacer:first-child,
  .round .spacer:last-child{ flex-grow:.5; }

  .round .game-spacer{
    flex-grow:1;
  }

/*
 *  General Styles
*/
    body{
    padding:10px;
    line-height:1.4em;
    color:white;
    }

    li.game{
    padding-left:20px;

    }

  li.game.winner{
    font-weight:bold;
  }
  li.game span{
    float:right;
    margin-right:5px;
  }

  li.game-top{ border-bottom:3px solid #aaa; }

  li.game-spacer{
    border-right:3px solid #aaa;
    min-height:40px;
  }

  li.game-bottom{
    border-top:3px solid #aaa;
  }

            .box {
            position: fixed;
            background: #546eff;
            bottom: 0;
            width: 98%;
            padding: 10px;
            margin: 0px;
            }

    /* .marquee {
    text-align: center;
    background: #546eff;
    
    color: #ffffff;
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
    white-space: nowrap;
    box-sizing: border-box;
    animation: marquee 15s linear infinite;
}


@keyframes marquee {
    0%   { text-indent: 100em }
    100% { text-indent: -100em }
} */


</style>
</head>
@endsection
@section('content')
<body>
@include('layouts.navbar')
<br><br><br><br><br><br>
<div class="flex-center position-ref full-height">
                {{-- <div class="top-right links">
                    <button type="button" class="btn btn-dark"><a href="{{ url('http://point-bracket.test/') }}"></a></button>
                </div> --}}

<main id="tournament">
	<ul class="round round-1">
		<li class="spacer">&nbsp;</li>
        @foreach($codm1s as $codm1)
            <li class="game game-top winner">{{$codm1->team_1}}</li>
            <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom winner">{{$codm1->team_2}}</li>

            <li class="spacer">&nbsp;</li>
        @endforeach
    </ul>

	<ul class="round round-2">
		<li class="spacer">&nbsp;</li>
        @foreach($codm2s as $codm2)
            <li class="game game-top winner">{{$codm2->team_1}}</li>
            <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom ">{{$codm2->team_2}}</li>

		    <li class="spacer">&nbsp;</li>
        @endforeach
    </ul>

	<ul class="round round-3">
		<li class="spacer">&nbsp;</li>
        @foreach($codm3s as $codm3)
            <li class="game game-top winner">{{$codm3->team_1}}</li>
            <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom ">{{$codm3->team_2}}</li>

        <li class="spacer">&nbsp;</li>
        @endforeach
    </ul>

	<ul class="round round-4">
		<li class="spacer">&nbsp;</li>
        @foreach($codm4s as $codm4)
            <li class="game game-top winner">{{$codm4->team_1}}</li>
            <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom ">{{$codm4->team_2}}</li>

            <li class="spacer">&nbsp;</li>
        @endforeach
    </ul>

	<ul class="round round-5">
		<li class="spacer">&nbsp;</li>
        @foreach($codm5s as $codm5)
            <li class="game game-top winner">{{$codm5->team_1}}</li>
            <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom ">{{$codm5->team_2}}</li>

            <li class="spacer">&nbsp;</li>
        @endforeach
    </ul>

    <ul class="round round-6">
		<li class="spacer">&nbsp;</li>
        @foreach($codm6s as $codm6)
        <li class="game game-top winner">
            <div class="row">
                <div class="col-md-2">
                    <img class="img-responsive" src="{{asset('../images/crown.png')}}" style="height:20px;width:20px">
                </div>
                <div class="col-md-10">
                    {{$codm6->team_1}}
                </div>
            </div>
        </li>
            {{-- <li class="game game-spacer">&nbsp;</li>
            <li class="game game-bottom ">{{$codm5->team_2}}</li> --}}

            <li class="spacer">&nbsp;</li>
        @endforeach
	</ul>

</div>


</main>
<!-- 
<div class="marquee">
        @foreach($commons as $common)
        <p>{{$common->text}} </p>
        @endforeach
    </div> -->

    <div class="box"><marquee direction="left" scrollamount="5" behavior="scroll" style="width: 100%; height: 15px; color: #ffffff; font-size: 13px; background-color: #546eff;">
    @foreach($commons as $common)
    <p>{{$common->text}} </p>
    @endforeach
    </marquee></div>
    <!-- <div class="box"><marquee direction="left" scrollamount="5" behavior="scroll" style="width: 100%; height: 15px; color: #ffffff; font-size: 13px; background-color: #546eff;">About US Portal Gaming Indonesia (PGID) adalah media yang menyediakan berbagai macam informasi menarik dan terupdate seputar dunia game dan teknologi PGID akan selalu memberikan konten yang menarik dan terpercaya  bagi Gaming People setiap harinya.Selain itu PGID juga akan selalu mengadakan turnamen sebagai salah satu wadah untuk Gaming People untuk menyalurkan dan menggali bakatnya dalam dunia game, seperti Tag Line kami yang berbunyi “Portal Gaming, dari gamers untuk gamers”. Terdapat beberapa kanal yang digunakan oleh PGID  untuk memberikan informasi dan konten inspiratif bagi Gaming People TAMAT END OF THE LINE</marquee></div> -->

</body>
@endsection
