<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/point', 'PointController@index');
Route::get('/point/tambah', 'PointController@tambah');
Route::post('/point/store', 'PointController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// GUEST
// freefire guest routes
Route::get('/guest/freefire', 'GuestFreefireController@index')->name('guest.freefire.index');


// pubg guest routes
Route::get('/guest/pubgm', 'GuestPubgmController@index')->name('guest.pubgm.index');

// pubg guest routes
Route::get('/guest/ml', 'GuestCodmController@index')->name('guest.codm.index');

// common guest routes
// Route::get('/guest/codm', 'GuestCodmController@index')->name('guest.codm.index');
// Route::get('/guest/codm', 'GuestCommonController@index')->name('guest.codm.index');


// ----------------------------------------------------------------------------------------------------------

// ADMIN
//admin common routes
Route::get('/common', 'CommonController@show')->name('admin.common.show');
Route::get('/common/create', 'CommonController@create')->name('admin.common.create');
Route::post('/common/post', 'CommonController@store')->name('admin.common.store');
Route::get('/common/{id}/edit', 'CommonController@edit')->name('admin.common.edit');
Route::patch('/common/{id}/edit', 'CommonController@update')->name('admin.common.update');
Route::delete('/common/{id}', 'CommonController@delete');

//admin freefire routes
Route::get('/freefire', 'FreeFireController@show')->name('admin.freefire.show');
Route::get('/freefire/create', 'FreeFireController@create')->name('admin.freefire.create');
Route::post('/freefire/post', 'FreeFireController@store')->name('admin.freefire.store');
Route::get('/freefire/{id}/edit', 'FreeFireController@edit')->name('admin.freefire.edit');
Route::patch('/freefire/{id}/edit', 'FreeFireController@update')->name('admin.freefire.update');
Route::delete('/freefire/{id}', 'FreeFireController@delete');


//admin pubgm routes
Route::get('/pubgm', 'PubgmController@show')->name('admin.pubgm.show');
Route::get('/pubgm/create', 'PubgmController@create')->name('admin.pubgm.create');
Route::get('/pubgm/tambah', 'PubgmController@tambah')->name('admin.pubgm.tambah');
Route::post('/pubgm/post', 'PubgmController@store')->name('admin.pubgm.store');
Route::get('/pubgm/{id}/edit', 'PubgmController@edit')->name('admin.pubgm.edit');
Route::patch('/pubgm/{id}/edit', 'PubgmController@update')->name('admin.pubgm.update');
Route::delete('/pubgm/{id}', 'PubgmController@delete');

//admin CODM ROUND 1 routes
Route::get('/ml1', 'Codm1Controller@show')->name('admin.codm1.show');
Route::get('/ml1/create', 'Codm1Controller@create')->name('admin.codm1.create');
Route::post('/ml1/post', 'Codm1Controller@store')->name('admin.codm1.store');
Route::get('/ml1/{id}/edit', 'Codm1Controller@edit')->name('admin.codm1.edit');
Route::patch('/ml1/{id}/edit', 'Codm1Controller@update')->name('admin.codm1.update');
Route::delete('/ml1/{id}', 'Codm1Controller@delete');

//admin CODM ROUND 2 routes
Route::get('/ml2', 'Codm2Controller@show')->name('admin.codm2.show');
Route::get('/ml2/create', 'Codm2Controller@create')->name('admin.codm2.create');
Route::post('/ml2/post', 'Codm2Controller@store')->name('admin.codm2.store');
Route::get('/ml2/{id}/edit', 'Codm2Controller@edit')->name('admin.codm2.edit');
Route::patch('/ml2/{id}/edit', 'Codm2Controller@update')->name('admin.codm2.update');
Route::delete('/ml2/{id}', 'Codm2Controller@delete');

//admin CODM ROUND 3 routes
Route::get('/ml3', 'Codm3Controller@show')->name('admin.codm3.show');
Route::get('/ml3/create', 'Codm3Controller@create')->name('admin.codm3.create');
Route::post('/ml3/post', 'Codm3Controller@store')->name('admin.codm3.store');
Route::get('/ml3/{id}/edit', 'Codm3Controller@edit')->name('admin.codm3.edit');
Route::patch('/ml3/{id}/edit', 'Codm3Controller@update')->name('admin.codm3.update');
Route::delete('/ml3/{id}', 'Codm3Controller@delete');

//admin CODM ROUND 4 routes
Route::get('/ml4', 'Codm4Controller@show')->name('admin.codm4.show');
Route::get('/ml4/create', 'Codm4Controller@create')->name('admin.codm4.create');
Route::post('/ml4/post', 'Codm4Controller@store')->name('admin.codm4.store');
Route::get('/ml4/{id}/edit', 'Codm4Controller@edit')->name('admin.codm4.edit');
Route::patch('/ml4/{id}/edit', 'Codm4Controller@update')->name('admin.codm4.update');
Route::delete('/ml4/{id}', 'Codm4Controller@delete');

//admin CODM ROUND 5 routes
Route::get('/ml5', 'Codm5Controller@show')->name('admin.codm5.show');
Route::get('/ml5/create', 'Codm5Controller@create')->name('admin.codm5.create');
Route::post('/ml5/post', 'Codm5Controller@store')->name('admin.codm5.store');
Route::get('/ml5/{id}/edit', 'Codm5Controller@edit')->name('admin.codm5.edit');
Route::patch('/ml5/{id}/edit', 'Codm5Controller@update')->name('admin.codm5.update');
Route::delete('/ml5/{id}', 'Codm5Controller@delete');

//admin CODM ROUND 6 routes
Route::get('/ml6', 'Codm6Controller@show')->name('admin.codm6.show');
Route::get('/ml6/create', 'Codm6Controller@create')->name('admin.codm6.create');
Route::post('/ml6/post', 'Codm6Controller@store')->name('admin.codm6.store');
Route::get('/ml6/{id}/edit', 'Codm6Controller@edit')->name('admin.codm6.edit');
Route::patch('/ml6/{id}/edit', 'Codm6Controller@update')->name('admin.codm6.update');
Route::delete('/ml6/{id}', 'Codm6Controller@delete');



